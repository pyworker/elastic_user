from elastic_user import ElasticUser
from time import sleep
from sys import exit

INDEX = 'user_verif'
TYPE = 'users'

if __name__ == '__main__':
    u = ElasticUser(INDEX, TYPE)
    print(u.add_user('node1178', 'artem'))
    print(u.add_local_user(
                hostname='fnode6722',
                username='bobo',
                chostname='node1189',
                cusername='artem'))
    exit(1)
