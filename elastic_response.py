from elastic_hit import ElasticHit

class ElasticResponse:
    """docstring for elastic"""
    def __init__(self, response):
        shards = response['_shards']
        self.successful = shards['successful']
        response = response['hits']
        self.total = response['total']
        self.hits = [ElasticHit(hit) for hit in response.get('hits')]