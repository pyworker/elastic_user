
class ElasticHit:
    """docstring for Hit"""
    def __init__(self, hit):
        self.id = hit.get('_id')
        self.version = hit.get('_version')
        self.source = hit.get('_source')
