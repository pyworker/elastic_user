import unittest

from elastic_user import ElasticUser
from elastic_storage import ElasticStorage

from time import sleep

INDEX = 'user_verif'
TYPE = 'users'
HOSTNAME = 'node1178'
HOSTNAME2 = 'fnode6722'
USERNAME = 'artem'
USERNAME2 = 'bobo'


class TestElasticUser(unittest.TestCase):
    """docstring for TestElasticUser"""
    def test_add_user(self):
        """
        currect work add user
        Raises: None
        """
        eu = ElasticUser(index=INDEX, type=TYPE)
        es = ElasticStorage(index=INDEX, type=TYPE)

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass

        # add new user
        gid = eu.add_user(hostname=HOSTNAME, local_username=USERNAME)
        sleep(1)
        should_ans = {
            'gid': gid,
            HOSTNAME: USERNAME
        }
        user = eu.find_user_local_username(hostname=HOSTNAME, local_username=USERNAME)
        ans = user.source
        self.assertDictEqual(ans, should_ans)

        try:
            # clean index after test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass

    def test_update_user(self):
        """
        currect work update user
        currect work add local user
        """
        eu = ElasticUser(index=INDEX, type=TYPE)
        es = ElasticStorage(index=INDEX, type=TYPE)

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass
        # add new user
        gid = eu.add_user(hostname=HOSTNAME, local_username=USERNAME)
        sleep(1)
        ans = eu.add_local_user(
                    hostname=HOSTNAME2,
                    username=USERNAME2,
                    chostname=HOSTNAME,
                    cusername=USERNAME)

        should_ans = {
            'gid': gid,
            HOSTNAME: USERNAME,
            HOSTNAME2: USERNAME2
        }

        user = eu.find_user_local_username(hostname=HOSTNAME, local_username=USERNAME)
        ans = user.source
        self.assertDictEqual(ans, should_ans)


if __name__ == '__main__':
    unittest.main()
