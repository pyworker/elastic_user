# coding=utf-8

import uuid 

from elastic_storage import ElasticStorage

class ElasticUser:

    def __init__(self, index, type):
        """pass"""
        self._eu = ElasticStorage(index=index, type=type)

    def add_user(self, hostname, local_username):
        """
        add new user to metaplan
        Args:
            hostname: str, dc hostname
            local_username: str, user 
        Returns:
            True: if user added
            False: if user dont added
        """

        # try to find user
        user = self.find_user_local_username(hostname, local_username)
        if user:
            print('user already exist. look at this user:\n %s' % user.source)
            return False
            
            print('change it?')

            # update already exist user
            self.update_user(
                global_id=user.id,
                hostname=hostname,
                local_username=local_username,
                version=user.version
            )
        else:
            # add new user
            print('add new user')
            gid = str(uuid.uuid4())
            user = {
                'gid': gid,
                hostname: local_username
            }

            gid = self._eu.add_with_id(document=user, gid=gid)
            if not gid:
                return None
        return gid

    def add_local_user(self, hostname, username, chostname, cusername):
        """
        add host and local_username to global user
        Args:
            hostname: str, dc hostname
            username: local username on dc
            chostname: str, current hostname
            cusername: str, current username
        Raises:
        Returns:
            True: info about local users added
            False: info about local users didnt add
        """
        user = self.find_user_local_username(
                        hostname=chostname,
                        local_username=cusername)

        # check that earlier map < hostname : username > 
        # doesnt use
        duplicate_user = self.find_user_local_username(
                                hostname=hostname,
                                local_username=username)
        if duplicate_user: 
            # stop work: find duplicated user
            print(
                'map < %s : %s > use for user\n %s'
                % (hostname, username, duplicate_user.source))
            return False

        if user:
            res = self.update_user(
                            global_id=user.id,
                            hostname=hostname,
                            local_username=username,
                            version=user.version)
            return res
        else:
            return False

    def update_user(self, global_id, hostname, local_username, version):
        """
        update already existing user
        Args:
            global_id: str, user global ID
            hostname: str, host name
            local_username: str, user name
            version: int or str, current version 
        Raises:
        Return:
            True: if user updated
            False: if users not updated
        """
        source_string = ('ctx._source.%s = params.local_id;' % hostname)
        query = {
            'script': {
                'source': source_string,
                'params': {
                    'local_id': local_username
            }}}

        self._eu.update(query=query, version=version, global_id=global_id)

    def find_user_local_username(self, hostname, local_username):
        """
        find user by local username
        Returns:
            Hit object: if find result
            None: if result not found 
        """
        search_query = {
            "version": True,
            "query": {
                "term": {hostname: local_username}
            }
        }

        res = self._eu.find(search_query=search_query)
        if res:
            return res[0]
        else:
            return None

    def del_user(self, hostname, local_username):
        """
        remove global user information
        Args:
            global_id: str, user's global_id
        Raises:
            ConnectionError: elastic hosts not available
            Exception: invalid args: global_id
        Returns:
            True: user is deleted 
        """
        if global_id:
            user = self.find_user_local_username(
                            hostname=hostname,
                            local_username=local_username)
        else:
            raise Exception('failed to delete user: invalid args: global_id')

            return self._eu.del_doc()



        # self._log.info('task with elastic_id=%s deleted' % elastic_id)
        return True if server_response.get('_shards').get('successful') else False


    def del_user_hostname(self, global_id, hostname):
        """
        remove user's username from DC
        Args:
            global_id: str, user's global_id
            hostname: str, datacenter id
        Raises:
            ConnectionError: elastic hosts not available
            Exception: elastic transport error
        Returns:
            True: if user is deleted
        """
        user = self.find_user_local_username(
                        hostname=chostname,
                        local_username=cusername)

        if not user:
            return False

        source_string = ('ctx._source.remove("%s")' % hostname)
        query = {
            'script': {
                'source': source_string,
               }}

        if not self.es.ping():
            raise ConnectionError('failed to update user %s: elastic hosts %s not available' % (local_username, HOSTS))
        try:
            server_response = self.es.update(
                index=USER_INDEX,
                doc_type=USER_TYPE,
                id=global_id,
                body=query,
                fields=["_source", "_id", "_version"],
                refresh=True,
                version=user.id
            )
            result = True if server_response.get('_shards').get('successful') else False

        except Exception as ex:
            raise Exception('failed to update_user: %s' % ex)
        return result